package com.hascode.tutorial;

import static com.jayway.restassured.RestAssured.expect;
import static org.hamcrest.Matchers.containsString;
import junit.framework.TestCase;

public class RestAssuredWithJava5Test extends TestCase {

	public void testSomePage() {
		expect().statusCode(200).body(containsString("micha kops")).when().get("http://www.hascode.com/");
	}

}
