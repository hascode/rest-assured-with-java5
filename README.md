# Short sample using rest-assured with Java 5

Simply run `mvn test` to execute the test.

For more detailed information about rest-assured as a framework to test RESTful webservices, please feel free to have a look at [my blog].

----

**2014 Micha Kops / hasCode.com**

   [my blog]:http://www.hascode.com/tag/rest
